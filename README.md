Setup

    Install the Google Cloud SDK (https://developers.google.com/cloud/sdk/).

Deploy

	Run Locally

		You can run the sample locally on XAMPP.

	Deploy with gcloud

	Open a command prompt, navigate to the folder with the code, and do the following: 

		gcloud config set project YOUR_PROJECT_ID
		gcloud app deploy
		gcloud app browse

The last command will open https://{YOUR_PROJECT_ID}.appspot.com/ in your browser. 