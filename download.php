<?php

/**
 * Title: download.php - part of "Frontend" script for Image Resize/Compression as a Service application
 * 
 * Date: June-July 2019
 * Authors: Mike MacDonald, with input from Ian Samuel
 * Code attributions: Google Cloud Client Libraries for PHP and other code attributions as 
 *	indicated in comments.
 * Developed for PHP 7.2 or later
 *
 * This script presents the client with a button (in an HTML form) to pull messages from Google Cloud Pub/Sub, 
 * 	output select information from any messages, and list URLs to all objects/files in Cloud Storage that have
 * 	the client's sessionId as a prefix in the objects' name. 

 * If there are no messages in the pub/sub subscription, an exception is caught and a message is
 * 	echoed to standard output.
 * 	
 */

session_start(); // start PHP session (visitor will have a session ID, the constant SID)

//Includes the autoloader for libraries installed with composer
require 'vendor/autoload.php';

// Imports the Google Cloud Storage client library.
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\PubSub\PubSubClient;

//Global variables
$projectId = 'frontend-php-20190614'; //Your Google Cloud Platform project ID
$bucketName = 'frontend-php-20190614.appspot.com'; // storage bucket name
$objectName = ' ';
$destination = 'D:/Mike/Downloads/temp/' . $objectName;
$messages_array = array();
$linksArray = array();

function auth_cloud_implicit($projectId){
    //Uncomment this block if running locally
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];
	
	//$config = [
    //    'projectId' => $projectId,
    //];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

    # Make an authenticated API request (listing storage buckets)
    foreach ($storage->buckets() as $bucket) {
        printf('Bucket: %s' . '<br>', $bucket->name());
    }
}

/**
 * Receive messages from Pub/Sub.
 *
 * @param string $projectId 
 * @param string $subscriptionName
 *
 * @return void 
 * See: https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php
*/
function pull_messages($projectId, $subscriptionName){
	
    $i = 0;
	
	$pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required to run locally
		//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
    ]);
	
    $subscription = $pubsub->subscription($subscriptionName);
	
    if ($subscription->exists()){
	
		foreach ($subscription->pull() as $message) {
			printf('Message (data and attributes): %s' . ' ', $message->data() . '<br>');
			//printf(' %s' . PHP_EOL, $message->attributes());
			$attributes = $message->attributes();
			//var_dump($attributes); // testing by dumping the entire array
			//echo '  name: ' . $attributes['name'] . '<br>';
			//echo '  bucket: ' . $attributes['bucket'] . '<br>';            
			//echo '  storageClass: ' . $attributes['storageClass'] . '<br>';
			//echo '  id: ' . $attributes['id'] . '<br>';
			//echo '  size: ' . $attributes['size'] . '<br>';
			//echo '  updated: ' . $attributes['updated'] . '<br>';
			//echo '  sessionId: ' . $attributes['sessionId'] . '<br>';
			
			// Acknowledge the Pub/Sub message has been received, so it will not be pulled multiple times.
			$subscription->acknowledge($message);
			
			$messages_array[$i] = $message;
			$i++;
		}    
		if(isset($messages_array)){
			return $messages_array; 
		} else throw new Exception('No messages to process.');

	} else throw new Exception('Subscription does not exist.');
}

/**
 * Download an object from Cloud Storage and save it as a local file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 * @param string $destination the local destination to save the encrypted object.
 *
 * @return void 
 * See: https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-php
 */
function download_object($projectId, $bucketName, $objectName, $destination){    
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
	
	//$storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($objectName);
	//$info = $object->info();
	//echo $info['size'];
    
	$object->downloadToFile($destination);
	
    printf('Downloaded %s to %s' . PHP_EOL, $object->gcsUri(), basename($destination));
}

/**
 * List all objects in a particular "folder" (i.e. that begin with a particular string in object name) 
 *   in a Cloud Storage bucket.
 * 
 *
 * @param string $projectId  The Google project ID.
 * @param string $bucketName The name of your Google Cloud bucket.
 * @param string $folderName The "folder" in the bucket; that is, the starting string of the object's name. 
 *
 * @return $linksArray An array of links. 
 * See: 
 */
function list_objects($projectId, $bucketName, $folderName){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
	
	//$storage = new StorageClient();
	$bucket = $storage->bucket($bucketName);
	
	//Get names of all objects in the bucket that start with the "folderName"	
	$objects = $bucket->objects([
		'prefix' => $folderName,
		'fields' => 'items/name,nextPageToken'		
	]);
	$i = 0;
	foreach ($objects as $object){
		$objectName = $object->name();
	//	echo $objectName . "<br>";
	//	echo "<a href='http://storage.googleapis.com/${bucketName}/${objectName}'"
	//			. "target='_blank'>" . $objectName . "</a>";
	//	echo "<a href=" . $object->signedUrl(new \DateTime('tomorrow')) 
	//			. "&response-content-disposition=attachment;>Download</a>";
	//	echo "<br>";
		// If object name contains $folderName  
		//if(strpos($objectName, $folderName) !== false){
			$linksArray[$i] = "<a href='http://storage.googleapis.com/${bucketName}/${objectName}'"
				. "target='_blank'>" . $objectName . "</a>";
	//			. "<a href=" . $object->signedUrl(new \DateTime('tomorrow')) 
	//			. "&response-content-disposition=attachment;>Download</a>"; // is not working when deployed to App Engine
			$i++;
		//}
	}
	echo count($linksArray) . ' links<br>';
	return $linksArray;
}

/**
 *"Main" program
 *
 */
echo '
<html>
	<head>
	</head>
	<body>
		<p><a href="/">Home</a>
		<p><h1>Image Resize/Compression Utility</h1>
		<br><br>
';

//Testing
	//echo '<p> List of storage buckets (testing):<br>';
	//auth_cloud_implicit('frontend-php-20190614');

echo '
		<!-- Form in a div element -->
			<div style="clear:both; padding-top: 1em;">
				<h3>Check status:</h3>
				<form style="color:000000" action="#" method="post">
				<input type="submit" value="CheckSub" name="CheckSub" />
				</form>
			</div>
';

//Pull messages from Cloud Pub/Sub, if any in the subscription, and if user clicked the button to check	
try {
	if(isset($_POST['CheckSub'])){
		echo '<br><br> Here we are pulling messages from the pub/sub subscription(back-to-front-sub): <br>';
		$messages_array = pull_messages('frontend-php-20190614', 'back-to-front-sub');

		// 
		foreach($messages_array as $message){
			$status = $message->data();
			$attributes = $message->attributes();
			$objectName = $attributes['name'];
			$bucketName = $attributes['bucket'];  
			
			echo '<br>';

			//Download the object/file using public URL	
			  // of the form http://storage.googleapis.com/[BUCKET_NAME]/[OBJECT_NAME]
			echo '<br><a href="http://storage.googleapis.com/' . $bucketName . '/' . $objectName 
				. '" target="_blank">' . "$objectName" . ' (Click to open)</a><br>';
			
		}			
	}
} catch (Exception $e){
	echo $e->getMessage() . '<br>';
}

//List files/objects in the bucket that have client's session ID in prefix
echo '<p>Here is a list of YOUR files in the bucket:<br>';
$linksArray = list_objects($projectId, $bucketName, session_id()); //function call
foreach($linksArray as $link){
	echo $link . "<br>";
}

echo '
	</body>
</html>
';

?>