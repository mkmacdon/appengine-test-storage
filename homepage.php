<?php

/**
 * Title: homepage.php - part of "Frontend" script for Image Resize/Compression as a Service application
 * 
 * Date: June-July 2019
 * Authors: Mike MacDonald, with input from Ian Samuel
 * Code attributions: Google Cloud Client Libraries for PHP, 
 * 	W3Schools.com (http://www.w3schools.com/php/php_file_upload.asp), and other code attributions as 
 *	indicated in comments.
 * Developed for PHP 7.2 or later
 *
 * This script presents the client with two (2) HTML forms:  i) accepts a file upload and selection of action 
 * 	to be performed on the file; and ii) a button to load another script, download.php
 * When the user uploads a file using the first form:
 *	A check is performed to ensure the user didn't click 'Upload' without specifying a file to upload; if 
 * 		this happened, the user is prompted with a message that they have to choose a file to upload.  	
 *
 * 	1. The file is checked to see if it is an image file (All image types supported by PHP, including 
 * 		JPEG, GIF, PNG, and more). A variable is set for use later
 * 		- If it is an image file, a message informing the user of this is outputted.
 * 		- If not an image file, a message informing the user of this is outputted.
 * 	2. An error check is performed.
 * 		- If an error with the upload is detected, a message informing the user 
 * 		of this is outputted.
 * 		- If no error with the upload is detected: 
 * 			- The file uploaded to Google Cloud Storage and a reference to the resulting
 * 				storage object is returned.
 * 			- The access permissions of the storage object in Cloud Storage is modified to make
 * 				it public.		
 *			- A message is written to Cloud Pub/Sub identifying the action being requested plus attributes about the 
 * 			storage object and the sessionId of the user.
 *			- The action being requested is output to the user.
 * When the user clicks the 'Download' button using the second form:
 * 	download.php is requested/executed.
 * 			
 * 
 */

session_start(); // start PHP session (visitor will have a session ID)
$_SESSION['sessionId'] = session_id(); // set a session variable with the PHP session ID

//Includes the autoloader for libraries installed with composer
require 'vendor/autoload.php';

//Imports the Google Cloud client library
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\PubSub\PubSubClient;

//Global variables
$projectId = 'frontend-php-20190614'; //Your Google Cloud Platform project ID
$bucketName = "frontend-php-20190614.appspot.com"; // storage bucket name
$uploadOk = 1; 
$messages_array = array();

/**
 * List object metadata.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 * @param string $objectName the name of your Cloud Storage object.
 *
 * @return void
 */
 // not using this; is for reference to know what attributes are available
function object_metadata($bucketName, $objectName){ 
    $storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($objectName);
    $info = $object->info();
    printf('Blob: %s' . PHP_EOL, $info['name']);
    printf('Bucket: %s' . PHP_EOL, $info['bucket']);
    printf('Storage class: %s' . PHP_EOL, $info['storageClass']);
    printf('ID: %s' . PHP_EOL, $info['id']);
    printf('Size: %s' . PHP_EOL, $info['size']);
    printf('Updated: %s' . PHP_EOL, $info['updated']);
    printf('Generation: %s' . PHP_EOL, $info['generation']);
    printf('Metageneration: %s' . PHP_EOL, $info['metageneration']);
    printf('Etag: %s' . PHP_EOL, $info['etag']);
    printf('Crc32c: %s' . PHP_EOL, $info['crc32c']);
    printf('MD5 Hash: %s' . PHP_EOL, $info['md5Hash']);
    printf('Content-type: %s' . PHP_EOL, $info['contentType']);
    printf("Temporary hold: " . ($info['temporaryHold'] ? "enabled" : "disabled") . PHP_EOL);
    printf("Event-based hold: " . ($info['eventBasedHold'] ? "enabled" : "disabled") . PHP_EOL);
    if ($info['retentionExpirationTime']) {
        printf("retentionExpirationTime: " . $info['retentionExpirationTime'] . PHP_EOL);
    }
    if (isset($info['metadata'])) {
        printf('Metadata: %s', print_r($info['metadata'], true));
    }
}

/**
 * Publishes a message for a Pub/Sub topic.
 *
 * @param string $projectId  The Google project ID.
 * @param string $topicName  The Pub/Sub topic name.
 * @param string $message  The message to publish.
 * 
 * See: 
 *	https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php 
 *	or https://cloud.google.com/pubsub/docs/publisher
 */
function publish_message($projectId, $topicName, $message, $attributes){
    $pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required for running locally
		//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
    ]);
    $topic = $pubsub->topic($topicName);
    $topic->publish([
        'data' => $message,
        'attributes' => $attributes
    ]);
    print('Message published' . '<br>');
	//print($topic); // testing; should get error that array can't be output at string
}

/**
 * Receive messages from Pub/Sub.
 *
 * @param string $projectId 
 * @param string $subscriptionName
 *
 * @return void 
 * See: https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php
*/
function pull_messages($projectId, $subscriptionName){
	
    $i = 0;
	
	$pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required to run locally
		//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
    ]);
	
    $subscription = $pubsub->subscription($subscriptionName);
	
    if ($subscription->exists()){
	
		foreach ($subscription->pull() as $message) {
			printf('Message (data and attributes): %s' . ' ', $message->data() . '<br>');
			//printf(' %s' . PHP_EOL, $message->attributes());
			$attributes = $message->attributes();
			//var_dump($attributes); // testing by dumping the entire array
			echo '  name: ' . $attributes['name'] . '<br>';
			echo '  bucket: ' . $attributes['bucket'] . '<br>';            
			echo '  storageClass: ' . $attributes['storageClass'] . '<br>';
			echo '  id: ' . $attributes['id'] . '<br>';
			echo '  size: ' . $attributes['size'] . '<br>';
			echo '  updated: ' . $attributes['updated'] . '<br>';
			echo '  sessionId: ' . $attributes['sessionId'] . '<br>';
			
			// Acknowledge the Pub/Sub message has been received, 
			//	so it will not be pulled multiple times.
			$subscription->acknowledge($message);
			
			$messages_array[$i] = $message;
			$i++;
		}    
		if(isset($messages_array)){
			return $messages_array; 
		} else throw new Exception('No messages to process.');

	} else throw new Exception('Subscription does not exist.');
}

/**
 * Upload an object to Cloud Storage.
 *
 * @param string $projectId  The Google project ID.
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 *
 * @return void 
 * See: https://cloud.google.com/storage/docs/uploading-objects 
  * and https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-php
 */
function upload_object($projectId, $bucketName, $object){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
	
	//$storage = new StorageClient();
	$bucket = $storage->bucket($bucketName);
	
	//Upload a file to the bucket
	$options = [
				//'name' => $objectName
				'name' => $_FILES['fileToUpload']['name']
				];
				
	$object = $bucket->upload(
				fopen($_FILES['fileToUpload']['tmp_name'], 'r'),
				$options
			);
	return $object;
	
}

/**
 * Make an object publicly accessible.
 *
 * @param string $bucketName the name of your Cloud Storage bucket.
 * @param string $objectName the name of your Cloud Storage object.
 * See: 
 * 	https://cloud.google.com/storage/docs/access-control/making-data-public#storage-make-object-public-php 
 *
 * @return void
 */
function make_public($projectId, $bucketName, $objectName){
	
    $config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
    
	$bucket = $storage->bucket($bucketName);
    
	$object = $bucket->object($objectName);
    
	$object->update(['acl' => []], ['predefinedAcl' => 'PUBLICREAD']);
    
	printf('gs://%s/%s is now public' . PHP_EOL, $bucketName, $objectName);
}

/**
 * List all objects in a Cloud Storage bucket.
 *
 * @param string $projectId  The Google project ID.
 * @param string $bucketName the name of your Google Cloud bucket.
 *
 * @return void 
 * See: 
 */
function list_objects($projectId, $bucketName){
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
	
	//$storage = new StorageClient();
	$bucket = $storage->bucket($bucketName);
	
	//Get names of all objects in the bucket	
	$objects = $bucket->objects([
		'fields' => 'items/name,nextPageToken'
	]);
	foreach ($objects as $object){
		echo $object->name() . "<br>";
	}

}

/**
 * Trigger the backend container script via HTTP.
 *
 *
 * @return void 
 * See: 
 *	https://codular.com/curl-with-php and https://cloud.google.com/appengine/docs/flexible/php/runtime 
 */
 function backend_ping(){
	 
	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, [
		CURLOPT_RETURNTRANSFER => 1,
		
		//use this if backend running on App Engine flex
		CURLOPT_URL => 'https://backend-docker-flex.appspot.com/', 
		
		// uncomment this if backend is running locally (Docker)
		//CURLOPT_URL => 'http://localhost:4000', 
		
	]);
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	// Close request to clear up some resources
	curl_close($curl);
	
	echo 'Backend triggered: ' . $resp . '<br>';
 
 }

/**
 *"Main" program
 *
 */
echo '
<html>
	<head>
	</head>
	<body>
		<p><a href="">Home</a>
		<p><h1>Image Resize/Compression Utility</h1>
		<!-- Form in a div element -->
		<div style="clear:both; padding-top: 1em;">
			<h3>Upload a file:</h3>
			<form style="color:000000" action="#" method="post" enctype="multipart/form-data">
			<!-- MAX_FILE_SIZE must precede the file input field -->
			<input type="hidden" name="MAX_FILE_SIZE" value="60000000" />
			<input type="file" name="fileToUpload" id="fileToUpload" >
			<p> Select an action: 
				<select name="action" id="action" required>
					<option value="resize_thumbnail" default>Resize (thumbnail)</option>
					<option value="resize_50">Resize (50%)</option>
					<option value="compress_85">Compress (85% quality)</option>
					<option value="resize_50_compress_85">Resize (50%) and Compress (85% quality)</option>
				</select>
				<br><br>
			<input type="submit" value="Upload" name="Upload" />
			</form>
		</div>
';

//Check for form submission
if(isset($_POST['Upload']) && $_FILES['fileToUpload']['tmp_name'] != NULL){
	
	//Check that file is an image
	$check = getimagesize($_FILES['fileToUpload']['tmp_name']);
		// Check if image file is a actual image or fake image
		if($check !== false){
			Print "<br>File is an image - " . $check['mime'] . ".<br>";
			$uploadOk = 1;
		} else {
			Print "<br>File is not an image.<br>";
			$uploadOk = 0;
		}
	
	//Check that no errors; if there is a problem, prompt to try again; else continue to commit file
	if($_FILES['fileToUpload']['error'] !== 0 || $uploadOk == 0){
		Print "<p>There was a problem with your upload.
					<p><a href='/'>Try again</a>.";
	} else 
		//Upload a file to the bucket
		// returns object reference to use elsewhere
		$object = upload_object($projectId, $bucketName, $_FILES['fileToUpload']); 
			
		//object_metadata($bucketName, $_FILES['fileToUpload']['name'])
		
		//Make the object publicly accessible
		make_public($projectId, $bucketName, $object->name()); 
	
		//Write a message to pub/sub with object metadata
		$info = $object->info();
		$attributes = [
            'name' => $info['name'],
			'bucket' => $info['bucket'],
			'storageClass' => $info['storageClass'],
			'id' => $info['id'],
			'size' => $info['size'],
			'updated' => $info['updated'],
			'sessionId' => $_SESSION['sessionId'] // user's PHP session variable
        ];
		if ($_POST['action'] != NULL){
			$message = $_POST['action'];
		} else $message = 'Image uploaded; no action requested.';
		publish_message($projectId, 'front-to-back-topic',$message, $attributes); //function call
		
		// Trigger the backend container script via HTTP
		backend_ping();
		
	//Output the action selected, if one was submitted.
	if ($_POST['action'] != NULL){
		echo '<p>Here is the action you want done to your file:  <em>' . $_POST['action'] . '</em><br>';
	}
} else {
	echo '
	You didn\'t upload a file. Please click the "Choose File" button and select a file to upload.

	';
}


echo '
	<!-- Form in a div element -->
		<div style="clear:both; padding-top: 1em;">
			<h3>Check status:</h3>
			<form style="color:000000" action="download.php" method="post">
			<input type="submit" value="Check/Download" name="CheckSub" />
			</form>
		</div>
		<p>Or use this link to go to another page to check status and download 
			file/object: <a href="download.php">Check/Download (download.php)</a>.
';

//List files/objects in the bucket - for testing
//echo '<p>Here is a list of ALL files in the bucket:<br>';
//list_objects($projectId, $bucketName); //function call

echo '
	</body>
</html>
';

?>