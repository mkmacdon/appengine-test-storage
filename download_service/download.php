<?php

//Includes the autoloader for libraries installed with composer
require 'vendor/autoload.php';

// Imports the Google Cloud Storage client library.
use Google\Cloud\Storage\StorageClient;
use Google\Cloud\PubSub\PubSubClient;

//Global variables
$projectID = 'frontend-php-20190614'; //Your Google Cloud Platform project ID
$bucketName = 'frontend-php-20190614.appspot.com'; // storage bucket name
$objectName = ' ';
$destination = 'D:/Mike/Downloads/temp/' . $objectName;

function auth_cloud_implicit($projectId){
    //Uncomment this block if running locally
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];
	
	//$config = [
    //    'projectId' => $projectId,
    //];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);

    # Make an authenticated API request (listing storage buckets)
    foreach ($storage->buckets() as $bucket) {
        printf('Bucket: %s' . '<br>', $bucket->name());
    }
}
/**
 * Receive messages from Pub/Sub.
 *
 * @param string $projectId 
 * @param string $subscriptionName
 *
 * @return void 
 * See: https://cloud.google.com/pubsub/docs/quickstart-client-libraries#pubsub-client-libraries-php
*/
function pull_messages($projectId, $subscriptionName){
    $pubsub = new PubSubClient([
        'projectId' => $projectId,
		//Can comment out the next line if deployed; required to run locally
		//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
    ]);
    $subscription = $pubsub->subscription($subscriptionName);
    foreach ($subscription->pull() as $message) {
        printf('<br>Message (data and attributes): <br> %s' . ' ', $message->data() . '<br> ');
		//printf(' %s' . '<br>', $message->attributes());
		$attributes = $message->attributes();
		//var_dump($attributes); // testing by dumping the entire array
		echo '  name: ' . $attributes['name'] . '<br>';
		echo '  bucket: ' . $attributes['bucket'] . '<br>';            
		echo '  storageClass: ' . $attributes['storageClass'] . '<br>';
		echo '  id: ' . $attributes['id'] . '<br>';
		echo '  size: ' . $attributes['size'] . '<br>';
		echo '  updated: ' . $attributes['updated'] . '<br>';
		
        // Acknowledge the Pub/Sub message has been received, so it will not be pulled multiple times.
        $subscription->acknowledge($message);
		
		// Pass message to another function to support other actions
		job($message);
	}    
}
/**
 * Download an object from Cloud Storage and save it as a local file.
 *
 * @param string $message a message from PubSub
 *
 * @return void 
 */
function job($message){
	$message->data();
	$attributes = $message->attributes();
	$objectName = $attributes['name'];
	$bucketName = $attributes['bucket'];  
	
	//download_object($projectID, $bucketName, $objectName, $destination);
}

/**
 * Download an object from Cloud Storage and save it as a local file.
 *
 * @param string $bucketName the name of your Google Cloud bucket.
 * @param string $objectName the name of your Google Cloud object.
 * @param string $destination the local destination to save the encrypted object.
 *
 * @return void 
 * See: https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-php
 */
function download_object($projectId, $bucketName, $objectName, $destination){    
	$config = [
			'projectId' => $projectId,
			//Can comment out the next line if deployed; required for running locally
			//'keyFilePath' => 'D:/Mike/OneDrive/MSCIS/COMP689/code/frontend-php-20190614-803c3809bab9.json'
		];

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    $storage = new StorageClient($config);
	
	//$storage = new StorageClient();
    $bucket = $storage->bucket($bucketName);
    $object = $bucket->object($objectName);
	//$info = $object->info();
	//echo $info['size'];
    $object->downloadToFile($destination);
    printf('Downloaded gs://%s/%s to %s' . PHP_EOL,
       $bucketName, $objectName, basename($destination));
}

//function createThumbnail($image){
//	header('Content-type: image/jpeg');
//
//	$image = new Imagick('image.jpg');
//
	// If 0 is provided as a width or height parameter,
	// aspect ratio is maintained
//	$image->thumbnailImage(100, 0);

//	echo $image;
//}

/**
 *"Main" program
 *
 */
echo '
<html>
	<head>
	</head>
	<body>
		<p><a href="index.php">Home</a>
		<br><br>
';

echo '<p> List of storage buckets (testing):<br>';
auth_cloud_implicit('frontend-php-20190614');

echo '
		<!-- Form in a div element -->
			<div style="clear:both; padding-top: 1em;">
				<h3>Check status:</h3>
				<form style="color:000000" action="#" method="post">
				<input type="submit" value="CheckSub" name="CheckSub" />
				</form>
			</div>
';

//Pull messages from Cloud Pub/Sub, if any in the subscription, and if user clicked the button to check	
if(isset($_POST['CheckSub'])){
	echo '<br><br> Here we are pulling messages from the pub/sub subscription: ';
	$subscription = pull_messages('frontend-php-20190614', 'my-sub');
}

echo '<br>';

echo '
		<!-- Form in a div element -->
			<div style="clear:both; padding-top: 1em;">
				<h3>Download:</h3>
				<form style="color:000000" action="#" method="post">
				<input type="submit" value="Download" name="Download" />
				</form>
			</div>
	</body>
</html>
';


//Download an object/file to local storage
$objectName = 'gingerbread men.jpg';
$destination = $destination . $objectName;
if(isset($_POST['Download'])){
	download_object($projectID, $bucketName, $objectName, $destination);
}



?>